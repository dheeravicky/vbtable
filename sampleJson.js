function GetSampleJson() {
    return [{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": false,
							"TailInfo": "",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"Supported": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportedMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"BoltGage": "GOL"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.25",
									"in": "2",
									"mm": 57.15,
									"fr_fra": "1/4"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"BoltStagger": "NA",
					"SupportSide": "SHOP_WELDED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "FIELD_BOLTED"
				}
			},
			"DirectlyWelded": {}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "SINGLE",
				"Profile": "L6X4",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "1"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "GOL"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "FIELD_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-1",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"ShearPlate": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"Wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "SINGLE",
				"Profile": "L6X4",
				"BoltDetails": {},
				"WeldDetails": {
					"wtype": "FILLET",
					"SealWeld": false,
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "8",
						"mm": 203.2,
						"fr_fra": "0"
					}
				},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "SHOP_WELDED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-2",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"BoltGage": "GOL"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "undefined",
									"in": "undefined",
									"mm": null,
									"fr_fra": ""
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.25",
									"in": "2",
									"mm": 57.15,
									"fr_fra": "1/4"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.25",
									"in": "5",
									"mm": 133.35,
									"fr_fra": "1/4"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"DirectlyWelded": {}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "DOUBLE",
				"Profile": "L6X4",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-3",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "END_PLATE",
			"EndPlate": {
				"Details": {
					"Grade": "A36",
					"Width": {
						"fr": "0.0",
						"ft": "1",
						"in": "1",
						"mm": 330.2,
						"fr_fra": "0",
						"Required": true
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Endplate": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"fr": "0.5",
								"in": "5",
								"mm": 139.7,
								"fr_fra": "1/2"
							},
							"EdgeDistance": {
								"Vertical": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								},
								"Horizontal": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "3"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						}
					},
					"Supported": {
						"WeldDetails": {
							"Wtype": "FILLET",
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							}
						}
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"SupportSide": "FIELD_BOLTED",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ClipAngle": {},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.25",
									"in": "5",
									"mm": 133.35,
									"fr_fra": "1/4"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"DirectlyWelded": {}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "DOUBLE",
				"Profile": "L6X4",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "4"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.25",
								"in": "1",
								"mm": 31.75,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-4",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.5",
									"in": "5",
									"mm": 139.7,
									"fr_fra": "1/2"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "undefined",
									"in": "undefined",
									"mm": null,
									"fr_fra": ""
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "DOUBLE",
				"Profile": "L6X4",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "1"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "GOL"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.25",
								"in": "1",
								"mm": 31.75,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "VERTICAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-5",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"ShearPlate": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"Wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "END_PLATE",
			"EndPlate": {
				"Details": {
					"Grade": "A36",
					"Width": {
						"fr": "0.0",
						"ft": "1",
						"in": "1",
						"mm": 330.2,
						"fr_fra": "0",
						"Required": false
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Endplate": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"fr": "0.5",
								"in": "5",
								"mm": 139.7,
								"fr_fra": "1/2"
							},
							"EdgeDistance": {
								"Vertical": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								},
								"Horizontal": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "3"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						}
					},
					"Supported": {
						"WeldDetails": {
							"Wtype": "FILLET",
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							}
						}
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"SupportSide": "FIELD_BOLTED",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ClipAngle": {},
			"DirectlyWelded": {}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "DOUBLE",
				"Profile": "L6X4",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.25",
								"in": "2",
								"mm": 57.15,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.25",
								"in": "1",
								"mm": 31.75,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "VERTICAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-6",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.25",
									"in": "5",
									"mm": 133.35,
									"fr_fra": "1/4"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "undefined",
									"in": "undefined",
									"mm": null,
									"fr_fra": ""
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {
				"Profile": "WT4",
				"ToeType": "VERTICAL",
				"BoltDetails": {},
				"WeldDetails": {
					"wtype": "FILLET",
					"SealWeld": false,
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "8",
						"mm": 203.2,
						"fr_fra": "0"
					}
				},
				"ConnectionMethod": "SHOP_WELDED",
				"FlangeCutClearance": {
					"fr": "0.0",
					"in": "1",
					"mm": 25.4,
					"fr_fra": "0"
				}
			},
			"HSS": {},
			"Angle": {},
			"BraceShape": "WT",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-8",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "SHEAR_PLATE",
			"EndPlate": {},
			"ClipAngle": {},
			"ShearPlate": {
				"Details": {
					"Grade": "A36",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"WeldDetails": {
							"AlongWidth": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							},
							"AlongLength": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							}
						}
					},
					"Supported": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Gusset": {
									"dhh": "STD"
								},
								"ShearPl": {
									"dhh": "STD"
								}
							},
							"MaxBoltEcc": {
								"Value": {
									"fr": "0.0",
									"ft": "",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Required": false
							},
							"EdgeDistance": {
								"Gusset": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								},
								"Vertical": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								},
								"Horizontal": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "3"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"ShearPlType": "CONVENTIONAL_SHEAR_TAB",
					"SupportSide": "SHOP_WELDED",
					"ShearPlDepth": "SIMPLE",
					"SupportedSide": "FIELD_BOLTED",
					"StiffenerPlate": {
						"Side": "",
						"Grade": "",
						"Required": false,
						"Thickness": {},
						"WeldDetails": {},
						"StiffenerNotConnectedToWeb": {}
					},
					"PlWeldDimensions": {
						"Width": {
							"fr": "0.0",
							"ft": "",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Length": {
							"fr": "0.0",
							"ft": "",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						}
					}
				}
			},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {
				"Profile": "W8",
				"SetBack": {
					"mm": 6.35,
					"os": "0.25",
					"os_fra": "1/4"
				},
				"ConnectionType": {
					"Web": {
						"Details": {
							"Type": "SPLICE_PLATE",
							"SupportSide": "FIELD_BOLTED",
							"SupportedSide": "SHOP_BOLTED"
						},
						"Required": true
					},
					"Flange": {
						"Details": {
							"Type": "CLAW_ANGLE",
							"SupportSide": "FIELD_BOLTED",
							"SupportedSide": "SHOP_BOLTED"
						},
						"Required": true
					}
				},
				"WebOrientation": "VERTICAL",
				"WebConectionDetails": {
					"ClawAngle": {},
					"SplicePlate": {
						"Type": "DOUBLE",
						"Grade": "A36",
						"Thickness": {
							"mm": 6.35,
							"tp": "0.25",
							"tp_fra": "1/4"
						},
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Brace": {
									"dhh": "STD"
								},
								"Gusset": {
									"dhh": "STD"
								},
								"SplicePlate": {
									"dhh": "STD"
								}
							},
							"EdgeDistance": {
								"Brace": {
									"fr": "0.0",
									"in": "1",
									"mm": 25.4,
									"fr_fra": "0"
								},
								"Gusset": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"SplicePlate": {
									"Vertical": {
										"fr": "0.0",
										"in": "1",
										"mm": 25.4,
										"fr_fra": "0"
									},
									"Horizontal": {
										"fr": "0.0",
										"in": "1",
										"mm": 25.4,
										"fr_fra": "0"
									}
								}
							},
							"NoofBoltRows": {
								"N": "undefined"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							}
						}
					}
				},
				"FlangeConectionDetails": {
					"ClawAngle": {
						"Grade": "A36",
						"Profile": "L4X4X1/4",
						"AngleOSL": "LONG_LEG",
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Brace": {
									"dhh": "STD"
								},
								"Gusset": {
									"dhh": "STD"
								},
								"ClawAngle": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Support": {
									"Value": {
										"fr": "0.0",
										"in": "0",
										"mm": 0,
										"fr_fra": "0"
									},
									"BoltGage": "SPECIFIC"
								},
								"Supported": {
									"Value": {
										"fr": "0.5",
										"in": "2",
										"mm": 63.5,
										"fr_fra": "1/2"
									},
									"BoltGage": "SPECIFIC"
								}
							},
							"EdgeDistance": {
								"Brace": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"Gusset": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"ClawAngle": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								}
							},
							"NoofBoltRows": {
								"N": "2"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "undefined",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							}
						}
					}
				}
			},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "W",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-9",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "SHEAR_PLATE",
			"EndPlate": {},
			"ClipAngle": {},
			"ShearPlate": {
				"Details": {
					"Grade": "A36",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"WeldDetails": {
							"AlongWidth": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							},
							"AlongLength": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							}
						}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							}
						}
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"ShearPlType": "CONVENTIONAL_SHEAR_TAB",
					"SupportSide": "SHOP_WELDED",
					"ShearPlDepth": "SIMPLE",
					"SupportedSide": "FIELD_WELDED",
					"StiffenerPlate": {
						"Side": "",
						"Grade": "",
						"Required": false,
						"Thickness": {},
						"WeldDetails": {},
						"StiffenerNotConnectedToWeb": {}
					},
					"PlWeldDimensions": {
						"Width": {
							"fr": "0.0",
							"ft": "",
							"in": "5",
							"mm": 127,
							"fr_fra": "0"
						},
						"Length": {
							"fr": "0.0",
							"ft": "",
							"in": "10",
							"mm": 254,
							"fr_fra": "0"
						}
					}
				}
			},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {
				"Profile": "W8",
				"SetBack": {
					"mm": 6.35,
					"os": "0.25",
					"os_fra": "1/4"
				},
				"ConnectionType": {
					"Web": {
						"Details": {
							"Type": "CLAW_ANGLE",
							"SupportSide": "FIELD_BOLTED",
							"SupportedSide": "SHOP_BOLTED"
						},
						"Required": true
					},
					"Flange": {}
				},
				"WebOrientation": "HORIZONTAL",
				"WebConectionDetails": {
					"ClawAngle": {
						"Grade": "A36",
						"Profile": "L4X4X1/4",
						"AngleOSL": "LONG_LEG",
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"Brace": {
									"dhh": "STD"
								},
								"Gusset": {
									"dhh": "STD"
								},
								"ClawAngle": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Support": {
									"Value": {
										"fr": "0.0",
										"in": "0",
										"mm": 0,
										"fr_fra": "0"
									},
									"BoltGage": "GOL"
								},
								"Supported": {
									"Value": {
										"fr": "0.5",
										"in": "2",
										"mm": 63.5,
										"fr_fra": "1/2"
									},
									"BoltGage": "SPECIFIC"
								}
							},
							"EdgeDistance": {
								"Brace": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"Gusset": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"ClawAngle": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								}
							},
							"NoofBoltRows": {
								"N": "2"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							}
						}
					},
					"SplicePlate": {}
				},
				"FlangeConectionDetails": {
					"ClawAngle": {}
				}
			},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "W",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-10",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.25",
									"in": "5",
									"mm": 133.35,
									"fr_fra": "1/4"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "undefined",
									"in": "undefined",
									"mm": null,
									"fr_fra": ""
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {
			"Type": "DIRECTLY_WELDED",
			"EndPlate": {},
			"ClipAngle": {},
			"DirectlyWelded": {
				"Details": {
					"TailInfo": "3 SIDES",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "8",
							"mm": 203.2,
							"fr_fra": "0"
						}
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {
				"Profile": "HSS6X4",
				"KnifePlate": {
					"Grade": "A36",
					"SetBack": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"BoltDetails": {
						"BoGr": "A325N",
						"BoltDia": {
							"d1": "0.75",
							"mm": 19.049999999999997,
							"d1_fra": "3/4"
						},
						"BoltName": "A325N",
						"HoleType": {
							"Gusset": {
								"dhh": "STD"
							},
							"KnifePlate": {
								"dhh": "STD"
							}
						},
						"EdgeDistance": {
							"Gusset": {
								"fr": "0.0",
								"in": "2",
								"mm": 50.8,
								"fr_fra": "0"
							},
							"KnifePlate": {
								"Vertical": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"Horizontal": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								}
							}
						},
						"NoofBoltRows": {
							"N": "2"
						},
						"BoltSpacingRows": {
							"Default": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							},
							"Maximum": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"Minimum": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"Increment": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"min_max_s": false
						},
						"NoofBoltColumns": {
							"N_C": "2"
						},
						"BoltSpacingColumns": {
							"Default": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						}
					},
					"SupportSide": "SHOP_BOLTED",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						}
					},
					"SupportedSide": "SHOP_WELDED"
				},
				"ConnectionType": "KNIFE_PLATE",
				"DirectlyWelded": {}
			},
			"Angle": {},
			"BraceShape": "HSS",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-12",
		"ConnectionType": "BEAM_AND_COLUMN",
		"GussetToColumn": {
			"Type": "CLIP_ANGLE",
			"EndPlate": {},
			"ClipAngle": {
				"Details": {
					"Grade": "A36",
					"Profile": "L2X2X1/8",
					"Setback": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Support": {
						"BoltDetails": {
							"BoGr": "A325N",
							"BoltDia": {
								"d1": "0.75",
								"mm": 19.049999999999997,
								"d1_fra": "3/4"
							},
							"BoltName": "A325N",
							"HoleType": {
								"ClipAngle": {
									"dhh": "STD"
								},
								"SupportMember": {
									"dhh": "STD"
								}
							},
							"GageDetails": {
								"Value": {
									"fr": "0.25",
									"in": "5",
									"mm": 133.35,
									"fr_fra": "1/4"
								},
								"BoltGage": "SPECIFIC"
							},
							"EdgeDistance": {
								"Angle": {
									"fr": "0.5",
									"in": "1",
									"mm": 38.099999999999994,
									"fr_fra": "1/2"
								}
							},
							"NoofBoltRows": {
								"N": "3"
							},
							"BoltSpacingRows": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								},
								"Maximum": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"Minimum": {
									"fr": "undefined",
									"in": "undefined",
									"mm": null,
									"fr_fra": ""
								},
								"Increment": {
									"fr": "0.0",
									"in": "0",
									"mm": 0,
									"fr_fra": "0"
								},
								"min_max_s": false
							},
							"NoofBoltColumns": {
								"N_C": "2"
							},
							"BoltSpacingColumns": {
								"Default": {
									"fr": "0.0",
									"in": "3",
									"mm": 76.19999999999999,
									"fr_fra": "0"
								}
							},
							"DistanceofFirstBolt": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						},
						"WeldDetails": {}
					},
					"Supported": {
						"BoltDetails": {},
						"WeldDetails": {
							"Wtype": "FILLET",
							"SealWeld": true,
							"TailInfo": "3 SIDES",
							"WeldSize": {
								"mm": 3.175,
								"weld": "0.125",
								"weld_fra": "1/8"
							},
							"WeldLength": {
								"fr": "0.0",
								"ft": "",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							}
						}
					},
					"BoltStagger": "NA",
					"SupportSide": "FIELD_BOLTED",
					"ClipAngleOSL": "SHORT_LEG",
					"SupportedSide": "SHOP_WELDED"
				}
			},
			"ShearPlate": {},
			"DirectlyWelded": {}
		}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {
			"WingPlate": {
				"Grade": "A36",
				"Thickness": {
					"mm": 1.5875,
					"tp": "0.0625",
					"tp_fra": "1/16"
				},
				"WeldDetails": {
					"Wtype": "FILLET",
					"TailInfo": "3SIDES",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					}
				}
			},
			"Stiffeners": {
				"Details": {
					"Grade": "A36",
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"WeldDetails": {
						"StiffenerToWeb": {
							"Wtype": "FILLET",
							"TailInfo": "3SIDES",
							"WeldSize": {
								"mm": 6.35,
								"weld": "0.25",
								"weld_fra": "1/4"
							}
						},
						"StiffenerToFlange": {
							"Wtype": "FILLET",
							"TailInfo": "3SIDES",
							"WeldSize": {
								"mm": 6.35,
								"weld": "0.25",
								"weld_fra": "1/4"
							}
						},
						"StiffenerToGusset": {
							"Wtype": "FILLET",
							"TailInfo": "3SIDES",
							"WeldSize": {
								"mm": 6.35,
								"weld": "0.25",
								"weld_fra": "1/4"
							}
						}
					}
				},
				"Required": true
			},
			"WeldDetails": {
				"Vertical": {
					"Wtype": "FILLET",
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "3",
						"mm": 76.19999999999999,
						"fr_fra": "0"
					}
				},
				"Horizontal": {
					"Wtype": "FILLET",
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "3",
						"mm": 76.19999999999999,
						"fr_fra": "0"
					}
				}
			},
			"ConnectionMethod": "FIELD_WELDED"
		},
		"KneeBrace": {},
		"GussetToBeam": {},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {
				"Profile": "HSS6X4",
				"KnifePlate": {
					"Grade": "A36",
					"SetBack": {
						"mm": 6.35,
						"os": "0.25",
						"os_fra": "1/4"
					},
					"Thickness": {
						"mm": 6.35,
						"tp": "0.25",
						"tp_fra": "1/4"
					},
					"BoltDetails": {
						"BoGr": "A325N",
						"BoltDia": {
							"d1": "0.75",
							"mm": 19.049999999999997,
							"d1_fra": "3/4"
						},
						"BoltName": "A325N",
						"HoleType": {
							"Gusset": {
								"dhh": "STD"
							},
							"KnifePlate": {
								"dhh": "STD"
							}
						},
						"EdgeDistance": {
							"Gusset": {
								"fr": "0.0",
								"in": "2",
								"mm": 50.8,
								"fr_fra": "0"
							},
							"KnifePlate": {
								"Vertical": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								},
								"Horizontal": {
									"fr": "0.0",
									"in": "2",
									"mm": 50.8,
									"fr_fra": "0"
								}
							}
						},
						"NoofBoltRows": {
							"N": "2"
						},
						"BoltSpacingRows": {
							"Default": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							},
							"Maximum": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"Minimum": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"Increment": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"min_max_s": false
						},
						"NoofBoltColumns": {
							"N_C": "2"
						},
						"BoltSpacingColumns": {
							"Default": {
								"fr": "0.0",
								"in": "3",
								"mm": 76.19999999999999,
								"fr_fra": "0"
							}
						}
					},
					"SupportSide": "SHOP_BOLTED",
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						}
					},
					"SupportedSide": "SHOP_WELDED"
				},
				"ConnectionType": "KNIFE_PLATE",
				"DirectlyWelded": {}
			},
			"Angle": {},
			"BraceShape": "HSS",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-13",
		"ConnectionType": "BASE_PLATE",
		"GussetToColumn": {}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Chevron": {
			"Details": {
				"Stiffeners": {
					"Required": true,
					"GussetEndStiffeners": {
						"Grade": "A36",
						"Width": {
							"Type": "",
							"Value": {
								"fr": "",
								"ft": "",
								"in": "",
								"mm": "",
								"fr_fra": ""
							}
						},
						"Thickness": {
							"mm": 6.35,
							"tp": "0.25",
							"tp_fra": "1/4"
						},
						"WeldDetails": {
							"StiffenerToWeb": {
								"wtype": "FILLET",
								"TailInfo": "3SIDES",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							},
							"StiffenerToFlange": {
								"wtype": "FILLET",
								"TailInfo": "3SIDES",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							},
							"StiffenerToGusset": {
								"wtype": "",
								"TailInfo": "undefined",
								"WeldSize": {
									"mm": "",
									"weld": "",
									"weld_fra": ""
								}
							}
						}
					},
					"IntermidiateStiffeners": {
						"Required": true,
						"StiffenerType": "",
						"NumberOfStiffeners": "2"
					}
				},
				"WeldDetails": {
					"wtype": "FILLET",
					"TailInfo": "3SIDES",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "10",
						"mm": 254,
						"fr_fra": "0"
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {},
		"Brace1toGusset": {
			"W": {},
			"WT": {
				"Profile": "WT4",
				"ToeType": "HORIZONTAL",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"ConnectionMethod": "SHOP_BOLTED",
				"FlangeCutClearance": {
					"fr": "0.0",
					"in": "0",
					"mm": 0,
					"fr_fra": "0"
				}
			},
			"HSS": {},
			"Angle": {},
			"BraceShape": "WT",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {
				"Profile": "WT4",
				"ToeType": "HORIZONTAL",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {
					"wtype": "FILLET",
					"SealWeld": false,
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "0",
						"mm": 0,
						"fr_fra": "0"
					}
				},
				"FlangeCutClearance": {
					"fr": "0.0",
					"in": "0",
					"mm": 0,
					"fr_fra": "0"
				}
			},
			"HSS": {},
			"Angle": {},
			"BraceShape": "WT",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-14",
		"ConnectionType": "CHEVRON",
		"GussetToColumn": {}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {
			"KB": {
				"Details": {
					"Stiffeners": {
						"Grade": "undefined",
						"Position": "GUSSET_END",
						"Required": true,
						"Thickness": {
							"mm": 6.35,
							"tp": "0.25",
							"tp_fra": "1/4"
						},
						"WeldDetails": {
							"StiffenerToWeb": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							},
							"StiffenerToFlange": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							},
							"StiffenerToGusset": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							}
						}
					},
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "1",
							"in": "6",
							"mm": 457.2,
							"fr_fra": "0"
						}
					},
					"WorkpointDetails": {
						"Type": "INSIDE_GUSSET",
						"InsideGusset": {
							"L1": {
								"fr": "0.0",
								"ft": "1",
								"in": "0",
								"mm": 304.79999999999995,
								"fr_fra": "0"
							},
							"L2": {
								"fr": "0.0",
								"ft": "1",
								"in": "2",
								"mm": 355.59999999999997,
								"fr_fra": "0"
							}
						},
						"OutsideGusset": {
							"L1": {
								"fr": "0.0",
								"ft": "1",
								"in": "0",
								"mm": 304.79999999999995,
								"fr_fra": "0"
							},
							"L2": {
								"fr": "0.0",
								"ft": "1",
								"in": "2",
								"mm": 355.59999999999997,
								"fr_fra": "0"
							}
						}
					},
					"WorkpointSpecific": true
				}
			}
		},
		"GussetToBeam": {},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "SINGLE",
				"Profile": "L2X2",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.25",
								"in": "2",
								"mm": 57.15,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.25",
								"in": "1",
								"mm": 31.75,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-15",
		"ConnectionType": "KNEE_BRACE",
		"GussetToColumn": {}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Baseplate": {},
		"KneeBrace": {
			"KB": {
				"Details": {
					"Stiffeners": {
						"Grade": "undefined",
						"Position": "AT_WORK_POINT",
						"Required": true,
						"Thickness": {
							"mm": 6.35,
							"tp": "0.25",
							"tp_fra": "1/4"
						},
						"WeldDetails": {
							"StiffenerToWeb": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							},
							"StiffenerToFlange": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							},
							"StiffenerToGusset": {
								"wtype": "FILLET",
								"TailInfo": "",
								"WeldSize": {
									"mm": 3.175,
									"weld": "0.125",
									"weld_fra": "1/8"
								}
							}
						}
					},
					"WeldDetails": {
						"wtype": "FILLET",
						"TailInfo": "",
						"WeldSize": {
							"mm": 3.175,
							"weld": "0.125",
							"weld_fra": "1/8"
						},
						"WeldLength": {
							"fr": "0.0",
							"ft": "1",
							"in": "6",
							"mm": 457.2,
							"fr_fra": "0"
						}
					},
					"WorkpointDetails": {
						"Type": "INSIDE_GUSSET",
						"InsideGusset": {
							"L1": {
								"fr": "0.0",
								"ft": "1",
								"in": "0",
								"mm": 304.79999999999995,
								"fr_fra": "0"
							},
							"L2": {
								"fr": "0.0",
								"ft": "1",
								"in": "2",
								"mm": 355.59999999999997,
								"fr_fra": "0"
							}
						},
						"OutsideGusset": {
							"L1": {
								"fr": "0.0",
								"ft": "1",
								"in": "0",
								"mm": 304.79999999999995,
								"fr_fra": "0"
							},
							"L2": {
								"fr": "0.0",
								"ft": "1",
								"in": "2",
								"mm": 355.59999999999997,
								"fr_fra": "0"
							}
						}
					},
					"WorkpointSpecific": true
				}
			}
		},
		"GussetToBeam": {},
		"Brace1toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {
				"Type": "SINGLE",
				"Profile": "L2X2",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "1",
							"mm": 25.4,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.25",
								"in": "2",
								"mm": 57.15,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.25",
								"in": "1",
								"mm": 31.75,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"AngleBackToBack": "HORIZONTAL",
				"ConnectionMethod": "SHOP_BOLTED"
			},
			"BraceShape": "L",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {},
			"HSS": {},
			"Angle": {},
			"BraceShape": "",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-16",
		"ConnectionType": "KNEE_BRACE",
		"GussetToColumn": {}
	},
	{
		"Gusset": {
			"Grade": "A36",
			"Thickness": {
				"mm": 6.35,
				"tp": "0.25",
				"tp_fra": "1/4"
			}
		},
		"Chevron": {
			"Details": {
				"Stiffeners": {
					"Required": true,
					"GussetEndStiffeners": {
						"Grade": "A36",
						"Width": {
							"Type": "",
							"Value": {
								"fr": "",
								"ft": "",
								"in": "",
								"mm": "",
								"fr_fra": ""
							}
						},
						"Thickness": {
							"mm": 6.35,
							"tp": "0.25",
							"tp_fra": "1/4"
						},
						"WeldDetails": {
							"StiffenerToWeb": {
								"wtype": "FILLET",
								"TailInfo": "3SIDES",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							},
							"StiffenerToFlange": {
								"wtype": "FILLET",
								"TailInfo": "3SIDES",
								"WeldSize": {
									"mm": 6.35,
									"weld": "0.25",
									"weld_fra": "1/4"
								}
							},
							"StiffenerToGusset": {
								"wtype": "",
								"TailInfo": "undefined",
								"WeldSize": {
									"mm": "",
									"weld": "",
									"weld_fra": ""
								}
							}
						}
					},
					"IntermidiateStiffeners": {
						"Required": true,
						"StiffenerType": "",
						"NumberOfStiffeners": "2"
					}
				},
				"WeldDetails": {
					"wtype": "FILLET",
					"TailInfo": "3SIDES",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "10",
						"mm": 254,
						"fr_fra": "0"
					},
					"ConnectionMethod": "SHOP_WELDED"
				}
			}
		},
		"Baseplate": {},
		"KneeBrace": {},
		"GussetToBeam": {},
		"Brace1toGusset": {
			"W": {},
			"WT": {
				"Profile": "WT4",
				"ToeType": "HORIZONTAL",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {},
				"ConnectionMethod": "SHOP_BOLTED",
				"FlangeCutClearance": {
					"fr": "0.0",
					"in": "0",
					"mm": 0,
					"fr_fra": "0"
				}
			},
			"HSS": {},
			"Angle": {},
			"BraceShape": "WT",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"Brace2toGusset": {
			"W": {},
			"WT": {
				"Profile": "WT4",
				"ToeType": "HORIZONTAL",
				"BoltDetails": {
					"BoGr": "A325N",
					"BoltDia": {
						"d1": "0.75",
						"mm": 19.049999999999997,
						"d1_fra": "3/4"
					},
					"BoltName": "A325N",
					"HoleType": {
						"Brace": {
							"dhh": "STD"
						},
						"Gusset": {
							"dhh": "STD"
						}
					},
					"EdgeDistance": {
						"Brace": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						},
						"Gusset": {
							"fr": "0.0",
							"in": "2",
							"mm": 50.8,
							"fr_fra": "0"
						}
					},
					"NoofBoltRows": {
						"N": "3"
					},
					"BoltSpacingRow": {
						"Default": {
							"fr": "0.0",
							"in": "3",
							"mm": 76.19999999999999,
							"fr_fra": "0"
						},
						"Maximum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Minimum": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"Increment": {
							"fr": "0.0",
							"in": "0",
							"mm": 0,
							"fr_fra": "0"
						},
						"min_max_s": false
					},
					"NoofBoltColumns": {
						"N_C": "2"
					},
					"BoltSpacingColumn": {
						"BoltColumn1": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltColumn2": {
							"Value": {
								"fr": "0.0",
								"in": "0",
								"mm": 0,
								"fr_fra": "0"
							},
							"BoltGage": "SPECIFIC"
						},
						"BoltCenterToCenter": {
							"Value": {
								"fr": "0.25",
								"in": "5",
								"mm": 133.35,
								"fr_fra": "1/4"
							},
							"BoltGage": "SPECIFIC"
						}
					}
				},
				"WeldDetails": {
					"wtype": "FILLET",
					"SealWeld": false,
					"TailInfo": "",
					"WeldSize": {
						"mm": 3.175,
						"weld": "0.125",
						"weld_fra": "1/8"
					},
					"WeldLength": {
						"fr": "0.0",
						"ft": "",
						"in": "0",
						"mm": 0,
						"fr_fra": "0"
					}
				},
				"FlangeCutClearance": {
					"fr": "0.0",
					"in": "0",
					"mm": 0,
					"fr_fra": "0"
				}
			},
			"HSS": {},
			"Angle": {},
			"BraceShape": "WT",
			"MaximumEccentricity": {
				"Value": {
					"fr": "",
					"ft": "",
					"in": "",
					"mm": "",
					"fr_fra": ""
				},
				"Required": false
			}
		},
		"ConnectionMark": "VB-17",
		"ConnectionType": "CHEVRON",
		"GussetToColumn": {}
	}
]
}